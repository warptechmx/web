

<?php
class Cache {

    const MODE_PROD = 'prod';
    const MODE_DEV = 'dev';

    private $dirCache = null;
    private $mode = 'dev';

    function __construct($base) {
        $this->dirCache = $base . '/var/cache';
    }

    private function compiladoRuta() {
        return "{$this->dirCache}/{$this->mode}";
    }

    private function comprobarDirectorio() {
        return is_dir($this->compiladoRuta());
    }

    private function rutaHistorial() {
        $dateTime = new \DateTime();
        return "{$this->compiladoRuta()}_{$dateTime->format('Y-m-dTHms')}";
    }

    function limpiarCache($mode) {
        $this->mode = $mode;
        if (!$this->comprobarDirectorio()) {
            return false;
        }
        return rename($this->compiladoRuta(), $this->rutaHistorial());
    }

}
?>
<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cache</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <style type="text/css">
        .container {
            margin: 10px auto;
            max-width: 480px;
            background-color: #F2F2F2;
            box-shadow: #CCC 5px 1px;
            border-radius: 5px;
            padding: 10px;
        }
        .container div {
            text-align: center;
        }
    </style>
</head>
<body>

<div class="container">
    <?php

    if (isset($_GET['mode'])):

        $mode = $_GET['mode'];
        $cache = new Cache(dirname(__DIR__));

        if ($mode == Cache::MODE_PROD && $cache->limpiarCache(Cache::MODE_PROD)) {
            echo "<div class=\"alert alert-success\" role=\"alert\">cache elminado en producción</div>";
        } else if ($mode == Cache::MODE_DEV && $cache->limpiarCache(Cache::MODE_DEV)) {
            echo "<div class=\"alert alert-success\" role=\"alert\">cache elminado en desarrollo</div>";
        } else {
            echo "<div class=\"alert alert-warning\" role=\"alert\">Ups!! ocurrio un problema</div>";
        }

    endif;

    ?>
    <div class="row">
        <div class="col">
            <h3>Cache</h3>
        </div>
        <div class="col"></div>
    </div>
    <div class="row">
        <div class="col"></div>
        <div class="col">
            <div class="btn-group" role="group" aria-label="Basic example">
                <?php
                echo '<a href="?mode=' . Cache::MODE_PROD  .'" class="btn btn-info">Producción</a>';
                echo '<a href="?mode=' . Cache::MODE_DEV . '" class="btn btn-dark">Desarrollo</a>';
                ?>
            </div>
        </div>
        <div class="col"></div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</body>
</html>
