<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/intranet")
 */
class IntranetController extends AbstractController {

	/**
	 * @Route("/revista", methods={"GET"}, name="intranet.revista");
	 */
	public function revista(Request $request): Response {
		return $this->render('intranet/revista.twig.html', [
			'seccion' => "Revista"
		]);
	}

	/**
	 * @Route("/federados", methods={"GET"}, name="get.home.federados");
	 */
	public function federados(Request $request): Response {
		return $this->render('intranet/federados.twig.html', [
			'seccion' => "Federados"
		]);
	}
}
