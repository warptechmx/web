<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

/**
 * @Route("/")
 */
class IndexController extends AbstractController {

	/**
	 * @Route("", methods={"GET"}, name="get.home");
	 */
	public function index(Request $request): Response {
		return $this->render('home/index.twig.html', [
			'seccion' => "Inicio"
		]);
	}

	/**
	 * @Route("/contacto", methods={"GET","POST"}, name="get.home.contacto");
	 */
	public function contacto(Request $request, MailerInterface $mailer): Response {
		$enviar = false;
		if ($request->getMethod() == 'POST') {
			$nombre = $request->request->get('nombre');
			$telefono = $request->request->get('telefono');
			$correo = $request->request->get('email');
			$mensaje = $request->request->get('mensaje');

			$enviar = true;
			if (empty($nombre) || empty($telefono) ||
				empty($correo) || empty($mensaje)) {
				$enviar = false;
			}

			if ($enviar) {
				$body = $this->renderView('home/contacto.mail.twig.html', [
					'email' => [
						'nombre' => $nombre,
						'telefono' => $telefono,
						'correo' => $correo,
						'mensaje' => $mensaje
					]
				]);

				$email = (new Email())
					->from($correo)
					->to('femecog@femecog.org.mx')
					//->cc('cc@example.com')
					//->bcc('bcc@example.com')
					->replyTo($correo)
					//->priority(Email::PRIORITY_HIGH)
					->subject('Contacto desde la pagina web')
					->text($mensaje)
					->html($body);

				$mailer->send($email);
			}
		}

		return $this->render('home/contacto.twig.html', [
			'seccion' => "Contacto",
			'enviado' => $enviar
		]);
	}

	/**
	 * @Route("/congresos-nacionales", methods={"GET"}, name="get.home.congresos-nacionales");
	 */
	public function congresos_nacionales(Request $request): Response {
		return $this->render('home/congresos-nacionales.twig.html', [
			'seccion' => "Congresos-nacionales"
		]);
	}

	/**
	 * @Route("/congresos-internacionales", methods={"GET"}, name="get.home.congresos-internacionales");
	 */
	public function congresos_internacionales(Request $request): Response {
		return $this->render('home/congresos-internacionales.twig.html', [
			'seccion' => "Congresos-internacionales"
		]);
	}

	/**
	 * @Route("/congresos-regionales", methods={"GET"}, name="get.home.congresos-regionales");
	 */
	public function congresos_regionales(Request $request): Response {
		return $this->render('home/congresos-regionales.twig.html', [
			'seccion' => "Congresos-regionales"
		]);
	}

	/**
	 * @Route("/apoyo-congresos", methods={"GET"}, name="get.home.apoyo-congresos");
	 */
	public function apoyo_congresos(Request $request): Response {
		return $this->render('home/apoyo-congresos.twig.html', [
			'seccion' => "Apoyo-Congresos"
		]);
	}

	/**
	 * @Route("/historia", methods={"GET"}, name="get.home.historia");
	 */
	public function historia(Request $request): Response {
		return $this->render('home/historia.twig.html', [
			'seccion' => "Historia"
		]);
	}

	/**
	 * @Route("/videoseries", methods={"GET"}, name="get.home.videoseries");
	 */
	public function videoseries(Request $request): Response {
		return $this->render('home/videoseries.twig.html', [
			'seccion' => "Videoseries"
		]);
	}

	/**
	 * @Route("/charlas_de_cafe", methods={"GET"}, name="get.home.charlas_de_cafe");
	 */
	public function charlas_de_cafe(Request $request): Response {
		return $this->render('home/charlas_de_cafe.twig.html', [
			'seccion' => "Charlas_de_cafe"
		]);
	}

	/**
	 * @Route("/femecog_invita", methods={"GET"}, name="get.home.femecog_invita");
	 */
	public function femecog_invita(Request $request): Response {
		return $this->render('home/femecog_invita.twig.html', [
			'seccion' => "Femecog_invita"
		]);
	}

	/**
	 * @Route("/comites", methods={"GET"}, name="get.home.comites");
	 */
	public function comites(Request $request): Response {
		return $this->render('home/comites.twig.html', [
			'seccion' => "Comites"
		]);
	}

	/**
	 * @Route("/beneficios", methods={"GET"}, name="get.home.beneficios");
	 */
	public function beneficios(Request $request): Response {
		return $this->render('home/beneficios.twig.html', [
			'seccion' => "Beneficios"
		]);
	}

	/**
	 * @Route("/carta", methods={"GET"}, name="get.home.carta");
	 */
	public function carta(Request $request): Response {
		return $this->render('home/carta.twig.html', [
			'seccion' => "Carta"
		]);
	}

	/**
	 * @Route("/constancias", methods={"GET"}, name="get.home.constancias");
	 */
	public function constancias(Request $request): Response {
		return $this->render('home/constancias.twig.html', [
			'seccion' => "Constancias"
		]);
	}

	/**
	 * @Route("/profesores", methods={"GET"}, name="get.home.profesores");
	 */
	public function profesores(Request $request): Response {
		return $this->render('home/profesores.twig.html', [
			'seccion' => "Profesores"
		]);
	}

	/**
	 * @Route("/expresidentes", methods={"GET"}, name="get.home.expresidentes");
	 */
	public function expresidentes(Request $request): Response {
		return $this->render('home/expresidentes.twig.html', [
			'seccion' => "Expresidentes"
		]);
	}

	/**
	 * @Route("/boletines", methods={"GET"}, name="get.home.boletines");
	 */
	public function boletines(Request $request): Response {
		return $this->render('home/boletines.twig.html', [
			'seccion' => "Boletines"
		]);
	}

	/**
	 * @Route("/comunidad", methods={"GET"}, name="get.home.comunidad");
	 */
	public function comunidad(Request $request): Response {
		return $this->render('home/comunidad.twig.html', [
			'seccion' => "Comunidad"
		]);
	}

	/**
	 * @Route("/regiones", methods={"GET"}, name="get.home.regiones");
	 */
	public function regiones(Request $request): Response {
		return $this->render('home/regiones.twig.html', [
			'seccion' => "Regiones"
		]);
	}

	/**
	 * @Route("/consejo", methods={"GET"}, name="get.home.consejo");
	 */
	public function consejo(Request $request): Response {
		return $this->render('home/consejo.twig.html', [
			'seccion' => "Consejo"
		]);
	}

	/**
	 * @Route("/infemecog", methods={"GET"}, name="get.home.infemecog");
	 */
	public function infemecog(Request $request): Response {
		return $this->render('home/infemecog.twig.html', [
			'seccion' => "Infemecog"
		]);
	}

	/**
	 * @Route("/plataformas", methods={"GET"}, name="get.home.plataformas");
	 */
	public function Plataformas(Request $request): Response {
		return $this->render('home/plataformas.twig.html', [
			'seccion' => "plataformas"
		]);
	}

	/**
	 * @Route("/proyectos-virtuales", methods={"GET"}, name="get.home.proyectos-virtuales");
	 */
	public function proyectos_virtuales(Request $request): Response {
		return $this->render('home/proyectos-virtuales.twig.html', [
			'seccion' => "Proyectos-Virtuales"
		]);
	}

	/**
	 * @Route("/ginecotips", methods={"GET"}, name="get.home.ginecotips");
	 */
	public function ginecotips(Request $request): Response {
		return $this->render('home/ginecotips.twig.html', [
			'seccion' => "Ginecotips"
		]);
	}

	/**
	 * @Route("/covid19", methods={"GET"}, name="get.home.covid19");
	 */
	public function covid19(Request $request): Response {
		return $this->render('home/covid19.twig.html', [
			'seccion' => "Covid19"
		]);
	}

	/**
	 * @Route("/webinars-2020", methods={"GET"}, name="get.home.webinars-2020");
	 */
	public function webinars_2020(Request $request): Response {
		return $this->render('home/webinars-2020.twig.html', [
			'seccion' => "Webinars-2020"
		]);
	}

	/**
	 * @Route("/videos-covid19", methods={"GET"}, name="get.home.videos-covid19");
	 */
	public function videos_covid19(Request $request): Response {
		return $this->render('home/videos-covid19.twig.html', [
			'seccion' => "Videos-Covid19"
		]);
	}

	/**
	 * @Route("/webinars-2021", methods={"GET"}, name="get.home.webinars-2021");
	 */
	public function webinars_2021(Request $request): Response {
		return $this->render('home/webinars-2021.twig.html', [
			'seccion' => "Webinars-2021"
		]);
	}

	/**
	 * @Route("/vacantes", methods={"GET"}, name="get.home.vacantes");
	 */
	public function vacantes(Request $request): Response {
		return $this->render('home/vacantes.twig.html', [
			'seccion' => "Vacantes"
		]);
	}

	/**
	 * @Route("/conferencias", methods={"GET"}, name="get.home.conferencias");
	 */
	public function conferencias(Request $request): Response {
		return $this->render('ismael/bio.twig.html', [
			'seccion' => "Bio de Ismael Gonzalez"
		]);
	}
}
